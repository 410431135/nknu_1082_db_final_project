# 資料庫期末專題

## About

108-2資料庫期末專題 -- **DB for Vehicle Company**

### 開發環境

`python 3.6.3 + django 2.2.6 + MySql`

### 文件

[Gitbook](https://410431135.gitlab.io/nknu_1082_db_final_project/)

### 版本控制

[Gitlab](https://gitlab.com/410431135/nknu_1082_db_final_project)

## 指導老師

葉道明 教授

## Authors

**林冠曄** 410431135

- 高雄國立師範大學 數學系數學組108級

**李昱珊** 410531131

- 高雄國立師範大學 數學系數學組109級

## Usage

###Setting up

1. Go to directory 'src/Automobile'.
2. Run ``python manage.py runserver`` in your commandline.
3. By default, there would be a website on http://127.0.0.1:8000
4. There's five button correspond to five functions.
5. Click and the detail shows up.
6. You can do some query mentioned in final project requirements and even generalize it.

![截圖 2020-07-01 下午11.28.43](home_screen_shot.png)

## ERD and Schema

see [About Database](SoftWareDesign/README.md)

## Sample Result

see [Sample Result](SampleResult/README.md)