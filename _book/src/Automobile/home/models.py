from django.db import models
import uuid

class Customer(models.Model):
    GENDER_CHOICES = (
        ('1', '男'),
        ('2', '女'),
		('3', '其他'),
    )
    CID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    C_name = models.CharField(verbose_name = '客戶名稱', max_length = 128, unique = True)
    address = models.TextField(verbose_name = '客戶地址', blank = True)
    phone = models.CharField(verbose_name = '客戶電話', max_length = 16, blank = True)
    gender = models.CharField(verbose_name = '客戶性別', max_length = 3, choices = GENDER_CHOICES, default = '3')
    annual_income = models.FloatField(verbose_name = '客戶年收入', blank = True)
	
    def __str__(self):
      return '%s' % (self.C_name)

class Dealer(models.Model):
    DID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    D_name = models.CharField(verbose_name = '經銷商名稱', max_length = 128)

    def __str__(self):
	    	return '%s' % (self.D_name)

class Brand(models.Model):
    B_name = models.CharField(verbose_name = '品牌名稱', max_length = 128, primary_key = True)

    def __str__(self):
	    	return '%s' % (self.B_name)

class Model(models.Model):
    STYLE_CHOICES = (
        ('1', 'SUV'),
        ('2', '4-door'),
        ('3', 'wagon')
    )
    MID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    M_name = models.CharField(verbose_name = '車型名稱', max_length = 128)
    body_style = models.CharField(verbose_name = '車體風格',max_length = 3, choices = STYLE_CHOICES,)
    B_name = models.ForeignKey(Brand, on_delete = models.CASCADE)

    def __str__(self):
		    return '%s' % (self.M_name)

class Supplier(models.Model):
    SID = models.CharField(verbose_name = '供應商名稱', max_length = 128, primary_key = True)

    def __str__(self):
	    	return '%s' % (self.SID)

class Manufacturer(models.Model):
    MFID = models.CharField(verbose_name = '製造商名稱', max_length = 128, primary_key = True)

    def __str__(self):
	    	return '%s' % (self.MFID)

class Engine(models.Model):
    EID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    SID = models.ForeignKey(Supplier, on_delete = models.SET_NULL, null=True)
    MFID = models.ForeignKey(Manufacturer, on_delete = models.SET_NULL, null=True)
    date = models.DateField(verbose_name = '製造日期')

    def __str__(self):
	    	return '%s' % (self.EID)

class Transmission(models.Model):
    TID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    SID = models.ForeignKey(Supplier, on_delete = models.SET_NULL, null=True)
    MFID = models.ForeignKey(Manufacturer, on_delete = models.SET_NULL, null=True)
    date = models.DateField(verbose_name = '製造日期')

    def __str__(self):
	    	return '%s' % (self.TID)

class Option(models.Model):
    OID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    color = models.CharField(verbose_name = '顏色', max_length = 128)
    engine = models.ForeignKey(Engine, on_delete = models.SET_NULL, null=True)
    transmission = models.ForeignKey(Transmission, on_delete = models.SET_NULL, null=True)
    MID = models.ForeignKey(Model, on_delete = models.SET_NULL, null=True)

    def __str__(self):
        return '%s' % self.color

class Vehicle(models.Model):
    VIN = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
    OID = models.ForeignKey(Option, on_delete = models.SET_NULL, null=True)

    def __str__(self):
		    return '%s' % (self.VIN)

class Sale(models.Model):
    VIN = models.ForeignKey(Vehicle, on_delete = models.CASCADE)
    CID = models.ForeignKey(Customer, on_delete = models.SET_NULL, blank = True, null=True)
    DID = models.ForeignKey(Dealer, on_delete = models.SET_NULL, null=True)
    date_purchase = models.DateField(verbose_name = '進貨日期')
    date_sold = models.DateField(verbose_name = '出售日期', blank = True, null = True)
    price = models.FloatField(verbose_name = '價錢', blank = True, null = True)

    def __str__(self):
		    return '進貨：%s 賣出：%s' % (self.date_purchase, self.date_sold)

class Produce(models.Model):
    VIN = models.ForeignKey(Vehicle, on_delete = models.CASCADE)
    MFID = models.ForeignKey(Manufacturer, on_delete = models.SET_NULL, null=True)
    date = models.DateField(verbose_name = '組裝日期')

    def __str__(self):
    		return '%s %s' % (self.VIN, self.date)
