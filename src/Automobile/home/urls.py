from django.urls import path
from . import views

urlpatterns = [
	path('', views.home, name='home'),
	path('search1', views.search1, name='search1'),
	path('search2', views.search2, name='search2'),
	path('search3', views.search3, name='search3'),
	path('search4', views.search4, name='search4'),
	path('search5', views.search5, name='search5'),
]