from django.contrib import admin
from .models import *

class VehicleInline(admin.TabularInline):
    model = Vehicle
    verbose_name = '車車'

class ModelInline(admin.TabularInline):
    model = Model
    verbose_name = 'Model'

class OptionInline(admin.TabularInline):
    model = Option
    verbose_name = '選項'

class ProduceInline(admin.TabularInline):
    model = Produce

class EngineInline(admin.TabularInline):
    model = Engine

class TransmissionInline(admin.TabularInline):
    model = Transmission

@admin.register(Dealer)
class Dealer(admin.ModelAdmin):
    pass

@admin.register(Vehicle)
class Vehicle(admin.ModelAdmin):
    inlines = [
        ProduceInline,
    ]

@admin.register(Brand)
class Brand(admin.ModelAdmin):
    inlines = [
        ModelInline,
    ]

@admin.register(Sale)
class Sale(admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass



@admin.register(Option)
class Option(admin.ModelAdmin):
    inlines = [
        # VehicleInline,
    ]

@admin.register(Model)
class Model(admin.ModelAdmin):
    inlines = [
        OptionInline,
    ]

@admin.register(Supplier)
class Supplier(admin.ModelAdmin):
    pass

@admin.register(Manufacturer)
class Manufacturer(admin.ModelAdmin):
    inlines = [
        EngineInline,
        TransmissionInline,
    ]

@admin.register(Transmission)
class Transmission(admin.ModelAdmin):
    pass

@admin.register(Engine)
class Engine(admin.ModelAdmin):
    pass

# @admin.register(Produce)
# class Produce(admin.ModelAdmin):
#     pass
