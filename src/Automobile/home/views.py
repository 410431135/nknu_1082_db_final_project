from django.shortcuts import render
from django.http import HttpResponse
from django import forms
from .models import *
from .forms import *
import datetime

def home(request):
	return render(request, 'home/home.html', locals())

def search1(request):
	# first: 零件壞掉
	# input: supplier_name, two dates, broken_part
	# return: VIN of that car, customer who brought it.
	act1form = Act1Form(request.POST)
	if act1form.is_valid():
		supplier_name = act1form.cleaned_data['supplier']
		act1form.supplier = act1form.cleaned_data['supplier']
		start_date = act1form.cleaned_data['sDate']
		act1form.sDate = act1form.cleaned_data['sDate']
		end_date = act1form.cleaned_data['fDate']
		act1form.fDate = act1form.cleaned_data['fDate']
		broken_part = request.POST.get('part')
		# broken_part = act1form.cleaned_data['part']
		# act1form.part = act1form.cleaned_data['part']

	if act1form.is_valid() and broken_part == 'transmission':
		transmission = Transmission.objects.filter(
			SID = Supplier.objects.get(SID = supplier_name)
			).filter(
			date__lte = end_date
			).filter(
			date__gte = start_date
			)
		option_list = Option.objects.none()
		for item in transmission:
			option_list = option_list.union(Option.objects.filter(transmission = item))
		VIN_list = Vehicle.objects.none()
		for item in option_list:
			VIN_list = VIN_list.union(Vehicle.objects.filter(OID = item))
		sale_list = Sale.objects.none()
		for item in VIN_list:
			sale_list = sale_list.union(Sale.objects.filter(VIN = item, date_sold__isnull = False))
		customer_list = [sale.CID for sale in sale_list]
		return render(request, 'home/search1.html', locals())
	if act1form.is_valid() and broken_part == 'engine':
		engine = Engine.objects.filter(
			SID = Supplier.objects.get(SID = supplier_name)
			).filter(
			date__lte = end_date
			).filter(
			date__gte = start_date
			)
		option_list = Option.objects.none()
		for item in engine:
			option_list = option_list.union(Option.objects.filter(engine = item))
		VIN_list = Vehicle.objects.none()
		for item in option_list:
			VIN_list = VIN_list.union(Vehicle.objects.filter(OID = item))
		sale_list = Sale.objects.none()
		for item in VIN_list:
			sale_list = sale_list.union(Sale.objects.filter(VIN = item, date_sold__isnull = False))
		customer_list = [sale.CID for sale in sale_list]
		# raise Exception()
		return render(request, 'home/search1.html', locals())		

	
	# second: best dealer
	# sold the most in the part

	# third: top n brand

	# forth: which month sell best
	# input: car_style

	# 5th: worst dealer
	return render(request, 'home/search1.html', locals())

def search2(request):
	today = datetime.date.today()
	dealer_list = Dealer.objects.all()
	dealer_dollar = {}
	if request.POST.get('dealerN'):
		number = request.POST.get('dealerN')
		for item in dealer_list:
			sale_list = Sale.objects.filter(DID = item, date_sold__range = [today - datetime.timedelta(days=365),today])
			dollar_list = sale_list.values_list('price', flat=True)
			sum_dollar = sum(dollar_list)
			dealer_dollar.update({item : sum_dollar})
		dealer_rank = [ item for item in sorted(dealer_dollar.items(), key=lambda d: d[1], reverse=True)]
		dealer_rank = dealer_rank[:int(number)]
	return render(request, 'home/search2.html', locals())

def search3(request):
	today = datetime.date.today()
	if request.POST.get('brandN'):
		number = request.POST.get('brandN')
		brand_list = Brand.objects.all()
		brand_sale = {}
		for brand in brand_list:
			model_list = Model.objects.none()
			model_list = Model.objects.filter(B_name = brand)
			option_list = Option.objects.none()
			for item in model_list:
				option_list = option_list.union(Option.objects.filter(MID = item))
			VIN_list = Vehicle.objects.none()
			for item in option_list:
				VIN_list = VIN_list.union(Vehicle.objects.filter(OID = item))
			sale_list = Sale.objects.none()
			for item in VIN_list:
				sale_list = sale_list.union(Sale.objects.filter(VIN = item, date_sold__range = [today - datetime.timedelta(days=365),today]))
			brand_sale.update({brand : sale_list.count()})
		brand_rank = [ item for item in sorted(brand_sale.items(), key=lambda d: d[1], reverse=True)]
		brand_rank = brand_rank[:int(number)]

	return render(request, 'home/search3.html', locals())

def search4(request):
	bodystyle = request.POST.get('bodystyle')
	model_list = Model.objects.filter(body_style = bodystyle)
	option_list = Option.objects.none()
	for item in model_list:
		option_list = option_list.union(Option.objects.filter(MID = item))
	VIN_list = Vehicle.objects.none()
	for item in option_list:
		VIN_list = VIN_list.union(Vehicle.objects.filter(OID = item))
	month_sale = []
	for month in range(1,13):
		sale_list = Sale.objects.none()
		for item in VIN_list:
			sale_list = sale_list.union(Sale.objects.filter(VIN = item, date_sold__month = month))
		n = sale_list.count()
		month_sale.append(n)
	sum_n = sum(month_sale) 
	best_month = month_sale.index(max(month_sale))+1
	
	return render(request, 'home/search4.html', locals())

def search5(request):
	number = request.POST.get('badD')
	today = datetime.date.today()
	dealer_list = Dealer.objects.all()
	bad_dealer = {}
	if request.POST.get('badD'):
		for dealer in dealer_list:
			sale_list = Sale.objects.filter(DID = dealer)
			time_list = []
			for item in sale_list:
				if item.date_sold:
					time_list.append((item.date_sold-item.date_purchase).days)
				else:
					time_list.append((today - item.date_purchase).days)
			bad_dealer.update({dealer : sum(time_list)/len(time_list)})
		worst_dealer = [ item for item in sorted(bad_dealer.items(), key=lambda d: d[1], reverse=True)]
		worst_dealer = worst_dealer[:int(number)]
	return render(request, 'home/search5.html', locals())