from django import forms
from home.models import *
from django.forms import CharField, ModelMultipleChoiceField, ModelChoiceField, DateField


class Act1Form(forms.Form):
    # PART_CHOICES = (
    #     ('engine', 'engine'),
    #     ('transmission', 'transmission'),
    # )
    # part = forms.ChoiceField(label = '零件', choices = PART_CHOICES)
    supplier = forms.ModelChoiceField(label = '供應商', queryset = Supplier.objects.all())
    sDate = forms.DateField(widget = forms.SelectDateWidget())
    fDate = forms.DateField(widget = forms.SelectDateWidget())
