# Sample Result

1. Suppose that it is found that `transmissions` made by supplier `Getrag` between two given dates are defective. Find the `VIN` of each car containing such a transmission and the `customer` to which it was sold. If your design allows, suppose the defective transmissions all come from only one of Getrag’s plants.

![截圖 2020-07-03 下午7.46.54](截圖 2020-07-03 下午7.46.54.png)

2. Find the` dealer` who has` sold the most` (by dollar-amount) in the `past year`.

![截圖 2020-07-03 下午7.47.35](截圖 2020-07-03 下午7.47.35.png)

3. Find the` top 2 brands` by unit sales in the `past year`.

![截圖 2020-07-03 下午7.48.13](截圖 2020-07-03 下午7.48.13.png)

4. In what `month(s)` do` SUVs sell best`?

![截圖 2020-07-03 下午7.52.24](截圖 2020-07-03 下午7.52.24.png)

5. Find those` dealers` who keep a vehicle in inventory for the `longest average time.`

![截圖 2020-07-03 下午7.48.40](截圖 2020-07-03 下午7.48.40.png)